import React, { useState, useEffect } from 'react';
import useStyles from '../styles';
import { Paper, Stepper, Step, StepLabel, Typography, CircularProgress, Divider, Button, CssBaseline } from '@material-ui/core';
import AddressForm from '../AdressForm';
import PaymentForm from '../PaymentForm';
import { commerce } from '../../../lib/commerce';
import { Link, useHistory } from 'react-router-dom';



const steps = ['Shipping address', 'Payment details'];



const Checkout = ({ cart, order, onCaptureCheckout, error }) => {

    //use css clasess from styles.jsx
    const classes = useStyles();  

    //define state for payment steps
    const [activeStep, setActiveStep] = useState(0);
    //define checkout tokent state
    const [checkoutToken, setCheckoutToken] = useState(null);
    //define shipping data checkout form  initial state
    const[shippingData, setShippingData] = useState({});
    //use history hook
    const history = useHistory();
    //payment process status
    const [isFinished, setIsFinished] = useState(false);

    useEffect(() => {
        //generate token on checkout access
        const generateToken = async() =>{

            try{
                //define cart ID and token type to generate checkout token
                const token = await commerce.checkout.generateToken(cart.id, { type: 'cart' });
                //set token to generated checkout token
                setCheckoutToken(token);
                
            }catch(error){
                //navigate to homepage if there is an error
                //history.push('/');
            }
        }
    
        generateToken();

    }, [cart]);

    //increment next step in checkout process
    const nextStep = () => setActiveStep( (prevActiveStep) => prevActiveStep + 1 );
    //decrement back step in checkout process
    const backStep = () => setActiveStep( (prevActiveStep) => prevActiveStep - 1 );

    //handle next submit button in checkout form
    const next = (data) => {

        //send shipping data after submit the checkout form
        setShippingData(data);

        console.log('next_data', data);
        //navigate next step os the process
        nextStep();
    };

    const timeout = () => {
        //timeout to check if process is finished
        setTimeout( ()=>{
            setIsFinished(true);
        },3000 )
    };


    let Confirmation = () => (order.customer ? (
        <>
          <div>
            <Typography variant="h5">Thank you for your purchase, {order.customer.firstname} {order.customer.lastname}!</Typography>
            <Divider className={classes.divider} />
            <Typography variant="subtitle2">Order ref: {order.customer_reference}</Typography>
          </div>
          <br />
          <Button component={Link} variant="outlined" type="button" to="/">Back to home</Button>
        </>
      ) : isFinished ?  (
        <>
          <div>
            <Typography variant="h5">Thank you for your purchase!</Typography>
            <Divider className={classes.divider} />
          </div>
          <br />
          <Button component={Link} variant="outlined" type="button" to="/">Back to home</Button>
        </>
      ) :(
        <div className={classes.spinner}>
          <CircularProgress />
        </div>
      ));

    const Form = () => activeStep === 0 
    ? <AddressForm checkoutToken={ checkoutToken } next={ next }/>
    : <PaymentForm shippingData={ shippingData} onCaptureCheckout={onCaptureCheckout} checkoutToken={checkoutToken} nextStep={nextStep} backStep={backStep} timeout={timeout}/>

    
    return (
        <>
            <CssBaseline />
            <div className={classes.toolbar} />
            <main className={classes.layout} >
                <Paper className={classes.paper}>
                    <Typography variant="h4" align="center">Checkout</Typography>
                    <Stepper activeStep={activeStep} className={classes.stepper}>
                        {
                            steps.map((step)=>(
                                <Step key={step}>
                                    <StepLabel>{step}</StepLabel>
                                </Step>
                            ) )
                        
                        }
                    </Stepper>
                    {  /*check which step the user in payment process*/}
                    { activeStep === steps.length ? <Confirmation /> : checkoutToken && <Form /> }
                </Paper>
            </main>
        </>
    )
}

export default Checkout
