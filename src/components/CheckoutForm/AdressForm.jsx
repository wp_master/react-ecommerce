import React, { useEffect, useState } from "react";
import { InputLabel, Select, MenuItem, Button, Grid, Typography,TextField} from "@material-ui/core";
import { useForm, FormProvider } from "react-hook-form";
import { Link } from "react-router-dom";

import { commerce } from "../../lib/commerce";

const AddressForm = ({ checkoutToken, next }) => {
  //shipment initial states
  const [shippingCountries, setShippingCountries] = useState([]);
  const [shippingCountry, setShippingCountry] = useState("");
  const [shippingSubdivisions, setShippingSubdivisions] = useState([]);
  const [shippingSubdivision, setShippingSubdivision] = useState("");
  const [shippingOptions, setShippingOptions] = useState([]);
  const [shippingOption, setShippingOption] = useState("");
  const [FirstName, setFirstName] = useState("");
  const [LastName, setLastName] = useState("");
  const [Address, setAddress] = useState("");
  const [Email, setEmail] = useState("");
  const [City, setCity] = useState("");
  const [Zip, setZip] = useState("");

  //use forms hook methods
  const methods = useForm();

  const countries_list = Object.entries(shippingCountries).map(
    ([code, name]) => ({ id: code, label: name })
  );
  const subdivisions = Object.entries(shippingSubdivisions).map(
    ([code, name]) => ({ id: code, label: name })
  );
  const options = shippingOptions.map((sO) => ({
    id: sO.id,
    label: `${sO.description} - (${sO.price.formatted_with_symbol})`,
  }));

  //fetch shipping countries list
  const fetchShippingCountries = async (checkoutTokenId) => {

    const { countries } = await commerce.services.localeListShippingCountries(
      checkoutTokenId
    );

    setShippingCountries(countries);
    setShippingCountry(Object.keys(countries)[0]);
  };

  const fetchSubDivisions = async (countryCode) => {
    const { subdivisions } = await commerce.services.localeListSubdivisions(
      countryCode
    );

    setShippingSubdivisions(subdivisions);
    setShippingSubdivision(Object.keys(subdivisions)[0]);

  };

  const fetchShippingOptions = async (
    checkoutTokenId,
    country,
    stateProvince = null
  ) => {
    const options = await commerce.checkout.getShippingOptions(
      checkoutTokenId,
      { country, region: stateProvince }
    );
    setShippingOptions(options);
    setShippingOption(options[0].id);
  };

  //load shipping countries with checkout token
  useEffect(() => {
    const checkoutTokenId = checkoutToken.id;
    //fetch countries list with token ID
    fetchShippingCountries(checkoutTokenId);
  }, []);
  //set subdivisions countries
  useEffect(() => {
    if (shippingCountry) {
      fetchSubDivisions(shippingCountry);
    }
  }, [shippingCountry]);

  //load shipping options
  useEffect(() => {
    if (shippingSubdivision) {
      //fetch shipping options data
      fetchShippingOptions(
        checkoutToken.id,
        shippingCountry,
        shippingSubdivision
      );
    }
  }, [shippingSubdivision]);

  return (
    <>
      <Typography variant="h6" gutterBottom>
        Shipping Address
      </Typography>
      <FormProvider {...methods}>
        <form
          onSubmit={methods.handleSubmit((data) =>
            next({
              ...data,
              FirstName,
              LastName,
              Address,
              Email,
              City,
              Zip,
              shippingCountry,
              shippingSubdivision,
              shippingOption,
            })
          )}
        >
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                required
                name="firstName"
                onChange={(e) => setFirstName(e.target.value)}
                label="First name"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                required
                name="LastName"
                onChange={(e) => setLastName(e.target.value)}
                label="last name"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                required
                name="address"
                onChange={(e) => setAddress(e.target.value)}
                label="Address"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                required
                name="email"
                onChange={(e) => setEmail(e.target.value)}
                label="E-mail"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                required
                name="city"
                onChange={(e) => setCity(e.target.value)}
                label="City"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                required
                name="zip"
                onChange={(e) => setZip(e.target.value)}
                label="ZIP/ Postal code"
              />
            </Grid>

            {/* Shipment select fields*/}
            <Grid item xs={12} sm={6}>
              <InputLabel> Shipping Country</InputLabel>
              <Select
                value={shippingCountry}
                fullWidth
                defaultValue=""
                onChange={(e) => setShippingCountry(e.target.value)}
              >
                {countries_list.map((country) => (
                  <MenuItem key={country.id} value={country.id}>
                    {country.label}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
            <Grid item xs={12} sm={6}>
              <InputLabel> Shipping Subdivision</InputLabel>
              <Select
                value={shippingSubdivision}
                defaultValue=""
                fullWidth
                onChange={(e) => setShippingSubdivision(e.target.value)}
              >
                {subdivisions.map((subdivision) => (
                  <MenuItem key={subdivision.id} value={subdivision.id}>
                    {subdivision.label}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
            <Grid item xs={12} sm={6}>
              <InputLabel> Shipping Options</InputLabel>
              <Select
                value={shippingOption}
                defaultValue=""
                fullWidth
                onChange={(e) => setShippingOptions(e.target.value)}
              >
                {options.map((option) => (
                  <MenuItem key={option.id} value={option.id}>
                    {option.label}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
          </Grid>
          <br />
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Button component={Link} to="/cart" variant="outlined">
              Back to Cart
            </Button>
            <Button type="submit" variant="contained" color="primary">
              Next
            </Button>
          </div>
        </form>
      </FormProvider>
    </>
  );
};

export default AddressForm;
