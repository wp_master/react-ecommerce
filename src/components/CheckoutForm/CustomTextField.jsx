import React from 'react';
import { useFormContext, Controller } from 'react-hook-form';
import { TextField, Grid } from '@material-ui/core';

const FormInput = ({ name, label, required }) => {

    //use form context hook for form
    const { control } = useFormContext();

    return (

        <Grid item xs={12} sm={6}>

             <Controller 
            control={control}
            fullWidth
            name={name}
            label={label}
            as={TextField}
            defaultValue=""
            render={({ field }) => (
          <TextField

            required
            label={label}
            name={name}
            placeholder={label}            
          />)}

            /> 
        </Grid>
    )
}

export default FormInput
