import React from 'react';
import { Typography, Button, Divider } from '@material-ui/core';
import { Elements, CardElement, ElementsConsumer } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import Review from './Review';
import stripeLogo from '../../assets/images/stripe.png';

//Stripe API
const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLIC_KEY);


const PaymentForm = ({ checkoutToken, shippingData, backStep, onCaptureCheckout, nextStep, timeout }) => {

    //handle form submit
    const handleSubmit = async (event, elements, stripe) => {

        //prevent form submit
        event.preventDefault();

        if(!stripe || !elements)return;
        //define strpe configuration
        const cardElement = elements.getElement(CardElement);

        const { error, paymentMethod } = await stripe.createPaymentMethod( { type: 'card', card: cardElement } );

        if(error){
            console.log(error);
            
        }else{

            //containing all order data
            const orderData ={

                    line_items: checkoutToken.live.line_items,
                    customer: { firstname: shippingData.FirstName, lastname: shippingData.LastName, email:shippingData.Email  },
                    shipping: {  
                        name: 'Primary', street: shippingData.Address, town_city:shippingData.City,
                        county_state: shippingData.shippingSubdivision, postal_zip_code:shippingData.Zip,
                        country:shippingData.shippingCountry
                },

                fulfillment: { shipping_method: shippingData.shippingOption },
                payment:{
                    gateway: 'stripe',
                    stripe: {
                        payment_method_id: paymentMethod.id
                    }
                }
            }
            console.log('orderData paymeny form',orderData)
            onCaptureCheckout(checkoutToken.id, orderData);
            //execute checkout timeout
            timeout();
            //navigate next steo in process
            nextStep();
        }
    };


    return (
        <>
            <Review checkoutToken={checkoutToken}/>
            <Divider />
            <Typography variant="h6" gutterBottom style={{ margin: '20px 0' }}>
                Payment method
            </Typography>
            <Elements stripe={ stripePromise }>
                <ElementsConsumer>
                    { ({ elements, stripe })=>(
                        <form onSubmit={ (e)=> handleSubmit(e, elements, stripe)  }>
                            <CardElement />
                            <br /> <br />
                            <div style={{  display:'flex', justifyContent:'space-between' }}>
                                <Button variant="outlined" onClick={ backStep }>Back</Button>
                                <Button type="submit" variant="contained" disabled={ !stripe } color="primary">
                                   Pay { checkoutToken.live.subtotal.formatted_with_symbol }
                                </Button>
                            </div>
                        </form>
                    ) }
                </ElementsConsumer>
                <br /> <br />
                <div style={{  display:'flex', justifyContent:'center' }}>
                    <img src={stripeLogo} alt="logo" height="60px" className="PaymentLogo"  style={{ display: "flex", justifyContent: "center" }} />
                </div>    
            </Elements>
        </>    
    )
}

export default PaymentForm
