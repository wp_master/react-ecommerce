import React from "react";
import { Grid } from "@material-ui/core";
//import { makeStyles } from '@material-ui/core/styles';
import Product from "./Product/Product";
import useStyles from './styles';

//product stab data
/*  const products = [
  { id: 1, name: "Shoes", description: "Runing shoes", price: "$5" ,image:'https://www.wigglestatic.com/product-media/103961970/Asics-Contend-6-GS-Running-Shoes-Running-Shoes-TUNA-BLUE-BLACK-AW20-1014A086-401-UK-2-5.jpg?w=1200&h=1200&a=7'},
  { id: 2, name: "MacBook", description: "Apple macbook", price: "$10" ,image:'https://www.espir.co.il/images/itempics/Z0Y6-7322_12052020113511_large.jpg'},
];  */

const Products = ({ products, onAddToCart }) => {

  

  const classes = useStyles();

  return (
    <main className={classes.content}>
    <div className={classes.toolbar} />
      <Grid container justify="center" spacing={4}>
        {products.map((product) => (
          <Grid item key={product.id} xs={12} sm={6} md={4} lg={3}>
            <Product product={product} onAddToCart={ onAddToCart }/>
          </Grid>
        ))}
        
      </Grid>
    </main>
  );
};

export default Products;
