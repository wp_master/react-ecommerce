import React, { useState, useEffect } from 'react';
import { Products,  Navbar, Cart, Checkout } from './components';
import { commerce } from './lib/commerce.js';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

//ECommerce Web Shop - Build & Deploy an Amazing App | React.js, Commerce.js, Stripe

const App = () => {
    //products list state
    const [products, setProducts] = useState([]);
    //cart list state
    const [cart, setCart] = useState({});
    //order state
    const [order, setOrder] = useState({});
    //error message state
    const[errorMessage, setErrorMessage] = useState('');


    //fetch products products list
    const fetchProducts = async() =>{
        //destructer data from the response
        const { data } = await commerce.products.list();

        setProducts(data);
    };


    //fetch cart items list
    const fetchCart = async() =>{

        //destructer data from the response
        const cart= await commerce.cart.retrieve();
        //set cart items list
        setCart(cart);

        
    };

    //Add items to cart
    const handleAddToCart = async(productId, quantity)=> {

        const item = await commerce.cart.add(productId, quantity);

        setCart(item.cart);

    };

    //quantity of single item
    const handleUpdateCartQty = async(productId, quantity)=>{
        //update new quantity
        const { cart } = await commerce.cart.update(productId, { quantity });
        //set new quantity
        setCart(cart);


    };

    //remove a single item from cart
    const handleRemoveFromCart = async( productId )=>{

        //remove specific item by productId
        const { cart } = await commerce.cart.remove(productId);

        setCart(cart);
    }; 
    //empty cart from all items
    const handleEmptyCart = async() =>{
        //empty cart from all values
        const { cart } = await commerce.cart.empty();
        //set cart as empty
        setCart(cart);
    };

    //refresh cart after checkout
    const refreshCart = async () =>{
        const newCart = await commerce.cart.refresh();

        setCart(newCart);
    };


    //handle checkout processs
    const handleCaptureCheckout = async (checkoutTokenId, newOrder) =>{

        try{
            const incomingOrder = await commerce.checkout.capture(checkoutTokenId, newOrder);
            //set order details
            setOrder(incomingOrder);
            //refresh cart after order
            refreshCart();

        }catch(error){
            setErrorMessage(error.data.error.message);
            
        }

    };

    //fetch products from commerce.js API on load
    useEffect(() => {

        fetchProducts();

        fetchCart();

    }, []);

    return (
        <Router>
        <div>
            <Navbar totalItems= { cart.total_items } />

            <Switch>
                <Route exact path="/">
                    <Products products={ products }  onAddToCart = { handleAddToCart }/>
                </Route>
                <Route exact path="/cart">
                    <Cart  cart={ cart }
                    handleUpdateCartQty = { handleUpdateCartQty }
                    handleRemoveFromCart = { handleRemoveFromCart }
                    handleEmptyCart = { handleEmptyCart }
                    
                     />
                </Route>

                <Route exact path="/checkout">
                    <Checkout cart={cart} order={order} 
                    onCaptureCheckout= {handleCaptureCheckout} error={errorMessage} />
                </Route>

            </Switch>
            
        </div>

        </Router>
    )
}

export default App
